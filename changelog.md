# Changelog
*educinfo-reseau*

Package pour l'apprentissage des notions sur les réseaux pour le site educinfo.

Tous les changements notables seront documentés dans ce fichier

Le format s'appuie sur [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
et ce projet adhère au [versionnement sémantique](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.0] first released version

### Added    
- main pages templates for activity
   - history
   - layer model
   - physical layer and ethernet
   - ip layer
   - transport layer tcp and udp
   - dns service
   - to be done : http and routing
 - main images
 - rip example graphic
 - routing graphics