<?php
/*
	TP sur les réseaux
*/
global $pages;

$menu_activite = array(
	"titre" => "Les réseaux - travaux pratiques",
	"contenu" => [
        'reseau_TP1',
        'reseau_TP1_flash'
	]
);

// pages des bases du javascript
$reseau_tp_pages = array(
	'reseau_TP1' => array(
		"template" => 'reseau/TP1.twig.html',
		"menu" => 'reseau_tp',
		'page_precedente' => NULL,
		'page_suivante' => 'reseau_TP1_flash',
		'titre' => 'Travaux pratiques en salle',
		"css" => 'ressources/reseau/assets/css/reseau.css'
	),
	'reseau_TP1_flash' => array(
		"template" => 'reseau/TP1_flash.twig.html',
		"menu" => 'reseau_tp',
		'page_precedente' => 'reseau_TP1',
		'page_suivante' => NULL,
		'titre' => 'Cartes Flash sur les commandes réseau.',
		"css" => 'ressources/reseau/assets/css/reseau.css'
	)
);

$pages = array_merge($pages, $reseau_tp_pages);