<?php

register_activity('reseau_terminale',array(
		'category'=>'NSIT',
		'section'=>'NSIThardos',
		'type'=>'url',
		'titre'=>'Les réseaux informatiques',
		'auteur'=>"Laurent COOPER",
		'URL'=>'index.php?page=reseau_intro&activite=reseau_terminale',
		'commentaire'=>"Connaissance des réseaux",
		'directory'=>'reseau_terminale',
		'icon'=>'fas fa-network-wired',
		'prerequis'=>NULL
	)
);