<?php

register_activity('reseau',array(
		'category'=>'NSI1',
		'section'=>'NSI1hardos',
		'type'=>'url',
		'titre'=>'Les réseaux informatiques',
		'auteur'=>"Laurent COOPER",
		'URL'=>'index.php?page=reseau_intro&activite=reseau',
		'commentaire'=>"Connaissance des réseaux",
		'directory'=>'reseau',
		'icon'=>'fas fa-network-wired',
		'prerequis'=>NULL
	)
);