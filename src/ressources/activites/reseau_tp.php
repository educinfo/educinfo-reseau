<?php

register_activity('reseau_tp',array(
		'category'=>'NSI1',
		'section'=>'unseen',
		'type'=>'url',
		'titre'=>'TP sur les réseaux informatiques',
		'auteur'=>"Laurent COOPER",
		'URL'=>'index.php?page=reseau_TP1&activite=reseau_tp',
		'commentaire'=>"Connaissance des réseaux",
		'directory'=>'reseau_tp',
		'icon'=>'fas fa-network-wired',
		'prerequis'=>NULL
	)
);