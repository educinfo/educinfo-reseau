<?php
/*

	Pages des tutoriel d'apprentissage de p5js
*/
global $pages;

// Bases du javascript
$menu_activite = array(
	"titre" => "Les réseaux",
	"contenu" => [
		'reseau_intro',
		'reseau_historique',
		'reseau_couches',
		'reseau_physique',
		'reseau_IP',
		'exercices_reseau_IP',
		'reseau_TCP_UDP',
		'reseau_DNS',
		'reseau_applicatif'
	]
);

// pages des bases du javascript
$intro_pages = array(
	'reseau_intro' => array(
		"template" => 'reseau/introduction.twig.html',
		"menu" => 'reseau',
		'page_precedente' => NULL,
		'page_suivante' => 'reseau_historique',
		'titre' => 'Introduction',
		"css" => 'ressources/reseau/assets/css/reseau.css'
	),
	'reseau_historique' => array(
		"template" => 'reseau/historique.twig.html',
		"menu" => 'reseau',
		'page_precedente' => 'reseau_intro',
		'page_suivante' => 'reseau_couches',
		'titre' => 'Historique',
		"css" => 'ressources/reseau/assets/css/reseau.css'
	),
	'reseau_couches' => array(
		"template" => 'reseau/couches.twig.html',
		"menu" => 'reseau',
		'page_precedente' => 'reseau_historique',
		'page_suivante' => 'reseau_physique',
		'titre' => 'Le modèle en couches',
		"css" => 'ressources/reseau/assets/css/reseau.css'
	),
	'reseau_physique' => array(
		"template" => 'reseau/physique.twig.html',
		"menu" => 'reseau',
		'page_precedente' => 'reseau_couches',
		'page_suivante' => 'reseau_IP',
		'titre' => 'Couche physique',
		"css" => 'ressources/reseau/assets/css/reseau.css'
	),
	'reseau_IP' => array(
		"template" => 'reseau/ip.twig.html',
		"menu" => 'reseau',
		'page_precedente' => 'reseau_physique',
		'page_suivante' => 'exercices_reseau_IP',
		'titre' => 'Réseaux IP',
		"css" => 'ressources/reseau/assets/css/reseau.css'
	),
	'exercices_reseau_IP' => array(
		"template" => 'reseau/exercices_ip.twig.html',
		"menu" => 'reseau',
		'page_precedente' =>'reseau_IP',
		'page_suivante' => 'reseau_TCP_UDP',
		'titre' => 'Exercices sur les réseaux IP',
		"css" => 'ressources/reseau/assets/css/reseau.css'
	),
	'reseau_TCP_UDP' => array(
		"template" => 'reseau/TCP_UDP.twig.html',
		"menu" => 'reseau',
		'page_precedente' => 'exercices_reseau_IP',
		'page_suivante' => 'reseau_DNS',
		'titre' => 'Les protocoles TCP et UDP',
		"css" => 'ressources/reseau/assets/css/reseau.css'
	),
	'reseau_DNS' => array(
		"template" => 'reseau/DNS.twig.html',
		"menu" => 'reseau',
		'page_precedente' => 'reseau_TCP_UDP',
		'page_suivante' => 'reseau_applicatif',
		'titre' => "le DNS",
		"css" => 'ressources/reseau/assets/css/reseau.css'
	),
	'reseau_applicatif' => array(
		"template" => 'reseau/applicatif.twig.html',
		"menu" => 'reseau',
		'page_precedente' => 'reseau_DNS',
		'page_suivante' => null,
		'titre' => 'Niveau applicatif',
		"css" => 'ressources/reseau/assets/css/reseau.css'
	)
);

$pages = array_merge($pages, $intro_pages);
