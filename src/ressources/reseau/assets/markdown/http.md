## La couche applicative : exemple du protocole HTTP. 
### Introduction

Le protocole HTTP (Hyper Text Transfert Protocol) est le protocole qui permet 
de faire des requêtes entre un client et un serveur web. C'est le protocole sur lequel
repose le World Wide Web (appelé plus communémement le web)

C'est dans le cadre de son travail sur la documentation interne du CERN que Tim Berners-Lee
invente à la fois le langage HTML, la notion d'adresse Web (les URL) et le protocole HTTP.

 - En mai 1996, la première version officielle de HTTP, HTTP/1.0 voit le jour et est décrite
    par la <a href="https://datatracker.ietf.org/doc/html/rfc1945" target="_blank"> RFC 1945 </a>
 - En janvier 1997, HTTP/1.1 devient finalement standard de l'IETF (internet Engineering Task Force). 
    Il est décrit dans la 
    <a href="https://datatracker.ietf.org/doc/html/rfc2068" target="_blank">RFC 2068</a> de l'IETF, 
    puis dans la 
    <a href="https://datatracker.ietf.org/doc/html/rfc2616" target="_blank">RFC 2616</a> en juin 1999. 
 -  En février 2014, la spécification de HTTP/1.1 a été republiée. 
    Elle a été éclatée en plusieurs RFC et corrigée pour toutes ses imprécisions, RFC 72304 à RFC 7237
 -  En février 2015, l'IETF approuve les 
    <a href="https://httpwg.github.io/specs/rfc7540.html" target="_blank">RFC 7540</a> et 7541 qui constituent 
    le protocole HTTP/2. Cette version 2 du protocole permet d'utiliser la compression, le multiplexage et la 
    priorisation, tout en maintenant essentiellement la compatibilité avec HTTP/1.1.

### Requêtes et réponses

Le protocole HTTP est basé sur un paradigme requête/réponse.Un client établit une connexion vers un serveur et envoie une requête HTTP, qui est suivie d'une réponse HTTP du serveur

#### Requêtes

La requête HTTP est envoyée par le client au serveur pour demander une ressource (par exemple une page web). La requête est composée de plusieurs éléments :

- L'adresse de la ressource demandée (par exemple http://www.example.com/index.html)
- La méthode de la requête (par exemple GET ou POST)
- Des informations supplémentaires telles que des paramètres ou des en-têtes

La méthode la plus courante est la méthode GET, qui permet de récupérer une ressource. Elle est utilisée pour les requêtes de type "lecture" : quand le client veut simplement afficher une page ou un document.

Voici un exemple de requête GET :

```http
GET /index.html HTTP/1.1
Host: www.example.com
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Accept-Encoding: gzip, deflate, br
Accept-Language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7
```

Dans cette requête, on demande la ressource "index.html" sur le serveur "www.example.com". On spécifie également quelques en-têtes supplémentaires, tels que l'agent utilisateur (User-Agent), 
qui indique le navigateur utilisé, et les types de contenu acceptés (Accept). 
Ces en-têtes permettent au serveur de mieux comprendre la demande du client et de renvoyer une réponse appropriée.

La méthode POST, quant à elle, permet de soumettre des données au serveur pour qu'il les traite. Elle est utilisée pour les requêtes de type "écriture" : quand le client veut envoyer des données pour les enregistrer sur le serveur.

Il existe d'autres méthodes de requête, telles que PUT (pour mettre à jour une ressource), DELETE (pour supprimer une ressource), HEAD (pour récupérer uniquement les en-têtes de la réponse) et OPTIONS (pour demander les options de communication disponibles pour la ressource demandée).


#### Réponses

Une fois la requête envoyée, le serveur renvoie une réponse HTTP au client. La réponse est composée de plusieurs éléments :

- Un code de statut (par exemple 200 OK, 404 Not Found)
- Des en-têtes contenant des informations supplémentaires sur la réponse
- Le corps de la réponse, qui contient la ressource demandée (par exemple une page web)

Les codes de statut HTTP sont des nombres à trois chiffres qui indiquent si la requête a été traitée avec succès ou non. Ils sont regroupés en familles selon leur première chiffre :

- Les codes de la famille 1xx indiquent une réponse de type "information", comme 100 Continue (le serveur continue de traiter la requête)
- Les codes de la famille 2xx indiquent une réponse de type "succès", comme 200 OK (la requête a été traitée avec succès)
- Les codes de la famille 3xx indiquent une réponse de type "redirection", comme 301 Moved Permanently (la ressource demandée a été déplacée)
- Les codes de la famille 4xx indiquent une réponse de type "erreur client", comme 404 Not Found (la ressource demandée n'a pas été trouvée)
- Les codes de la famille 5xx indiquent une réponse de type "erreur serveur", comme 500 Internal Server Error (le serveur a rencontré une erreur)

#### Les requêtes induites 

Lorsqu'une réponse HTTP renvoie une page HTML, cette page peut contenir des liens vers d'autres ressources, telles que des fichiers CSS, des images ou des scripts JavaScript. Le navigateur doit alors récupérer ces ressources supplémentaires en envoyant de nouvelles requêtes HTTP au serveur.

Prenons un exemple concret. Supposons qu'une page web contienne le code HTML suivant :

```html
<html>
  <head>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <h1>Bienvenue sur mon site web !</h1>
    <img src="image.jpg" alt="Une belle image">
    <script src="script.js"></script>
  </body>
</html>
```

Ce code contient des liens vers trois ressources supplémentaires : le fichier "style.css" pour la feuille de style, le fichier "image.jpg" pour l'image et le fichier "script.js" pour le script JavaScript.

Lorsque le navigateur récupère cette page en envoyant une requête HTTP GET au serveur, il analyse cette page. 
Il doit ensuite récupérer ces trois ressources supplémentaires en envoyant de nouvelles requêtes HTTP GET pour chacune d'entre elles. 
Le navigateur doit alors suivre les liens pour récupérer ces ressources et mettre en page la page web de manière appropriée.

Cela peut être visualisé comme une arborescence de requêtes HTTP, où la requête principale pour la page web déclenche des requêtes supplémentaires pour les ressources associées : 
<img src="ressources/reseau/assets/img/http-01.png" srcset="ressources/reseau/assets/img/http-01.svg" width="500px" class="d-block mx-auto img-fluid rounded">

:::retenir

- Le protocole http est le protocole qui gère les communications sur le World Wide Web.
- Il fonctionne sur un modèle client/serveur avec des requêtes et des réponses.
- Une réponse peut induire d'autres requêtes.
- La requête contient un en-tête et éventuellement des paramètres
- La réponse contient un en-tête et un contenu.
- Dans l'en-tête de la réponse, on trouve un code de réponse sur 3 chiffres:
    - 1xx pour les informations
    - 2xx pour les succès
    - 3xx pour les déplacements
    - 4xx pour les erreurs clients
    - 5xx pour les erreurs serveurs

:::
