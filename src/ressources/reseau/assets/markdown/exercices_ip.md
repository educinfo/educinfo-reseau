## Exercices sur les réseaux IP

### Masques alignés sur 8 bits
Lorsque l'on traite les réseaux, pour des raisons historiques et pratiques, les masques de réseau
sont souvent alignés sur des multiples de 8 bits. Cela signifie que l'on travaille par octet complet, ce qui peut simplifier le travail.

:::todo
Dominique vient d'installer la box de son fournisseur d'accès internet et a relié un premier ordinateur.
La commande <code>ip addr</code> lui a permis d'obtenir l'adresse IP de sa machine. Elle affiche
<code>192.168.1.10/24</code>

 1. Que signifie ce <code>/24</code> ?
 2. Donnez le masque de sous-réseau sous forme de notation décimale pointée  

En utilisant wireshark, Dominique constate que sur son ordinateur arrive des paquets venant de la machine 
dont l'IP est <code>192.168.0.9</code>. 
 
 3. En utilisant le masque de sous-réseau, indiquez si la machine est ou n'est pas sur le même sous-réseau que celle de Dominique.
 4. Donnez en la justifiant,  l'adresse IP du réseau de Dominique
 5. Quelle est l'adresse de broadcast du réseau ?
 6. Quelle est le nombre d'adresse disponible sur ce réseau pour des périphériques ?

:::

:::correction

 1. Le <code>/24</code> est le masque de sous réseau. Il indique que le masque de sous-réseau est composé de 24 bits à 1, suivi donc de 8 bits à 0.
 1. le masque en notation décimale pointée est <code>255.255.255.0</code> (il y a 24 bits à 1, soit 3 octets complets)
    <img src="ressources/reseau/assets/img/masque-01.png" srcset="ressources/reseau/assets/img/masque-01.svg" width="500px" class="d-block mx-auto img-fluid rounded">
 2. En appliquant le masque sur l'adresse de la machine, on otient 192.168.1.0. 
    <img src="ressources/reseau/assets/img/masque-02.png" srcset="ressources/reseau/assets/img/masque-02.svg" width="500px" class="d-block mx-auto img-fluid rounded">
    En appliquant le masque sur la seconde adresse, on obtient 192.168.0.0
   <img src="ressources/reseau/assets/img/masque-03.png" srcset="ressources/reseau/assets/img/masque-03.svg" width="500px" class="d-block mx-auto img-fluid rounded">
   Les deux adresses sont différentes, ce qui signifie que les deux machines ne sont pas sur le même sous-réseau.

   *N.B. : Il y a une technique plus rapide pour appliquer les masques de sous-réseau lorsqu'ils ont une longueur qui est un multiple de 8
   En effet, cela signifie que ce sont des octets complets qui sont à 1 au début, et à 0 à la fin. Dans cet exemple, le masque commence par
   trois octets ne contenant que des 1 et ensuite un octet ne contenant que des 0. Si un octet ne contient que des 1, le résultat du ET bit à
   bit est identique au départ (comme ici pour le 192, le 168 et le 1). Pour le dernier octet, comme il n'y a que des 0, le ET logique bit 
   à bit ne donnera que des 0. On peut donc donner l'adresse réseau immédiatement : <code>192.168.1.0</code> car les 3 premiers octets 
   sont inchangés et le dernier est mis à 0.*

 3. On a donné l'adresse IP du sous-réseau de Dominique en appliquant le masque de sous-réseau 
   à la question précédente : <code>192.168.1.0</code>
 4. En mettant à 1 les 8 derniers bits correspondant à la partie variable du réseau, on obtient l'adresse de broadcast qui est donc 
      <code>192.168.1.255</code>
 5. L'adresse de réseau et l'adresse de broadcast ne sont pas utilisable. Il y a 8 bits variables. 
      Il y a donc \\(2^8 - 2\\) adresses disponibles, soit 254 adresses (qui iront de 
      <code>192.168.1.1</code> à <code>192.168.1.254</code>)
:::

### Masques non alignés sur 8 bits

Dans des environnements plus complexes qu'un simple réseau de particulier, avec des sous-réseaux imbriqués, on peut rencontrer des masques qui ne 
sont pas alignés sur 8 bits. Les opérations sont alors un peu plus complexes, puisqu'il va falloir faire les opérations bit à bit sur la partie
qui n'est pas alignée. Les professionnels du réseau ont quelques astuces (basées sur la taille des blocs) pour aller plus vite, que l'on abordera pas ici. On se contentera du principe général.

:::todo
Sur la machine d'un utilisateur d'un lycée, on lance la commande <code>ip addr</code>. Les trois premières lignes de la sortie 
pour la carte filaire ethernet sont les suivantes :
```text
eth1:  mtu 1500 qdisc pfifo_fast state UP qlen 1000
	link/ether 00:50:56:39:13:ad brd ff:ff:ff:ff:ff:ff
	inet 172.21.203.14/21 brd 172.21.207.255 scope global eth1
```
 1. Que signifie à votre avis le <code>state UP</code> de la première ligne ?
 2. que représente le <code>00:50:56:39:13:ad</code> de la seconde ligne ?
 3. Que représente le <code>/21</code> sur la 3ème ligne ?
 4. Donnez le masque de sous-réseau en notation décimale pointée.
 5. Donnez l'adresse du réseau, l'adresse de broadcast, et le nombre de périphériques (orcinateur, imprimante, etc.) que l'on peut installer sur le réseau
 6. Un ordinateur personnel est connecté au WIFI de l'établissement. Son adresse IP est <code>172.21.210.13/21</code>. Est il sur le même réseau que la machine de l'élève ?
 7. Donner l'adresse du réseau de cet ordinateur personnel ainsi que l'adresse de broadcast de ce réseau

:::

:::correction

 1. Le <code>state UP</code> signifie que la carte réseau est connectée et dispose d'une adresse réseau.
 2. Le <code>00:50:56:39:13:ad</code> représente l'adresse matérielle (*adresse MAC*) de la carte réseau de l'ordinateur.
 3. le <code>/21</code> représente la longueur du masque de sous-réseau en notation CIDR.
 4. Le masque a une longueur de 21 bits. Cela signifie que les 21 premiers bits sont à 1, et les 11 derniers à 0.
 Les deux premiers octets ne contiennent que des 1, donc en notation décimale, cela fait 255. Le troisième octet contient 
 5 fois 1 puis 3 fois 0. <code>1111 1000</code> en binaire, cela fait 248 en décimal (*astuce : on peut trouver la réponse 
 en considérant que l'on a tous les bits à 1 sauf les 3 derniers, donc 255 - 4 - 2 - 1*)
  1. On va utiliser le masque de sous réseau pour trouver l'adresse du réseau et celle de broadcast. Pour le réseau :
   <img src="ressources/reseau/assets/img/masque-04.png" srcset="ressources/reseau/assets/img/masque-04.svg" width="500px" class="d-block mx-auto img-fluid rounded">
   L'adresse réseau est donc <code>172.21.200.0</code>
   On obtient l'adresse de broadcast en mettant les 11 derniers bits à 1 :
    <img src="ressources/reseau/assets/img/masque-05.png" srcset="ressources/reseau/assets/img/masque-05.svg" width="500px" class="d-block mx-auto img-fluid rounded">
   L'adresse de broadcast est donc <code>172.21.207.255</code>. On pouvait d'ailleurs la voir dans la 3ème ligne de la sortie de la commande.
   
   Comme la longueur du masque est de 21 bits, la partie variable est de 11 bits. Il y a donc \\(2^{11} - 2\\) soit 2046 adresses.
 1. La dernière adresse du réseau est celle de broadcast(<code>172.21.207.255</code>). 
   L'adresse de la machine est au dessus de cette adresse. La machine connectée en WIFI n'est donc pas connecté sur le même réseau que
   la machine de l'élève.
 2. En procédant de la même façon, on trouve comme adresse de réseau <code>172.21.208.0</code> et l'adresse de broadcast est 
   <code>172.21.215.255</code>
:::
 
:::retenir
 - une adresse MAC est l'adresse matérielle. Elle est donnée sur 6 octets en héxadécimal séparés par ":".
 - une adresse IP est donnée sur le réseau.
 - un masque de réseau peut être donnée en notation CIDR (la longueur du masque, le nombre de 1) ou en 
 notation décimale pointée.
 - le masque permet de savoir quelle est l'adresse du réseau d'une machine.
 - pour connaître l'adresse du réseau, on fait un ET logique bit à bit entre l'adresse de la machine et le masque
 - pour connaître l'adresse de broadcast, on met à 1 tous les bits de la partie mobile du réseau
 - si le masque a une longueur de \\(n\\), la partie mobile a une longueur de \\(32-n\\)
 - sur un réseau, si la partie mobile est de taille \\(k\\), on peut mettre dessus \\(2^k-2\\) machines (le nombre
 total d'adresse, moins celle du réseau et celle de broadcast).
:::